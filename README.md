# Software Studio 2021 Spring Midterm Project

## Topic
* Project Name : Midterm_Project_108000104
## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：[xxxx]
https://mid-project-a2874.web.app
## Website Detail Description
跳通知部分我試過chrome 和microsoft edge和firefox，但只有firefox有，希望助教測試通知的時候使用firefox，其他畫面用chrome會比較好(google signin firefox會當掉但另外兩個可以)。
大致講解一下，最一開始進入的是public chatroom，是大家都可在上面聊天，再來若想進入私人聊天室有兩個方式，一個是先創新的聊天室(點creat chatroom)再去choose chatroom，另一個是直接去(點choose chatroom)可選以前的聊天室，而右邊多的(my count、count setting、other count information)是我額外多做的功能，signin和signout是點account就會有選單跑出來了。

# Components Description : 

 
Basic Components
1. [Membership Mechanism] :
有三個選項，可以註冊新帳號，然後註冊完可以登錄。
![](https://i.imgur.com/PcYQefH.png)

 
2. [Firebase Page] :有使用firebase deploy
3. [Database] : 在public room 就有讀和寫的功能 
![](https://i.imgur.com/NyNSnsM.png)

5. [RWD] :有做不同畫面size的設計
 ![](https://i.imgur.com/LV8zP5j.png)

4. [Topic Key Function] :
有分別做出public room跟private room，並且可以隨使用者新增聊天室，chatroom name跟user都可由使用者更改(在creatroom.html)，填完資料後按下submit就可點選上面的choose chatroom，去選擇你想去的聊天室。![](https://i.imgur.com/7EG3XRf.png)

在 choose chatroom中，上面會顯示兩種你可以選擇的聊天室，第一種是你自己創建的，第二種是別人創建，但有把你加入使用者當中，而在全部你可選擇的聊天室，我給他們編號(chatumber)，只要在下方輸入數字，就會跳出小通知，按確認可前往private聊天室。 ![](https://i.imgur.com/Nb2YaOF.png)

private room是每個聊天室分開的，也只有被創建者加入的user可以進入聊天室看到資訊，並在裡面聊天。![](https://i.imgur.com/ntVr41H.png)

 
Advanced Components
1. [Third-Party Sign In] : google登入
 ![](https://i.imgur.com/SVPIhpj.png)

2. [Chrome Notification] : 在choose chatroom的頁面，有放一個butten，點test notification，就會跑出右下角通知。
 ![](https://i.imgur.com/HMG0sn3.png)

3. [Use CSS Animation] : 在一開始登入畫面我有放動畫。
 ![](https://i.imgur.com/H91l8ma.png)

4. [Security Report] : 在public跟private都可以
 ![](https://i.imgur.com/b2j8ruf.png)


# Other Functions Description : 
1. [聊天室的進入方式] :不是單純的像是尋找聊天室名稱就進入，這樣相對起來比較有private的感覺，能在私人聊天室裡的成員都是創建者自己加入的。
2. [my count] : 點my count後面進入像是自我介紹的畫面，而裡面的文字內容可不斷更新，更新部分我放在count setting，而在my count 比較特別的是，可以上傳照片，當作是你的大頭貼，可以不斷更新，且下面會顯示你所有曾傳過的照片，所以也可以當作是相簿使用。
 ![](https://i.imgur.com/CNCqUvB.png)

3. [count setting] :在count setting中，可以無限次更改會顯示在剛剛my count裡的資訊，像是可以取名字，自我介紹，生日，電話等等。![](https://i.imgur.com/j17N6Kc.png)

 4. [other count information] :點other count information後，會進入到一個畫面，你可以輸入其他人的email,然後點submit，就會跳到那個人的自我介紹畫面，包含他的大頭照片和一些他有輸入資訊。
![](https://i.imgur.com/P3CbzWd.png)
![](https://i.imgur.com/FurjjYV.png)



