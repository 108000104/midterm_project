function init() {
    var user_email = '';
    var url = location.href;
    
    //取得問號之後的值
    var chatnameinput = url.split("?");
    document.getElementById('roomname').innerHTML= chatnameinput[1];
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML =   user.email + "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                    window.location.href = "signin.html";
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);
            var countbtn = document.getElementById('count_but');
            countbtn.addEventListener("click", function() {
                window.location.href = "signin.html";
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var Ref = firebase.database().ref('private');
            var data = {
                chatname : chatnameinput[1],
                data: post_txt.value,
                email: user_email,
                time : getTime()
            };
            Ref.push(data);
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'></h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('private');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                first_count += 1
                if(childData.chatname==chatnameinput[1]){ 
                    total_post[total_post.length] = str_before_username+"<strong>" + childData.time+"</strong>"+  "<br>"+childData.email+ "</br>"+childData.data + str_after_content;
                }
            });


            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {

                    var childData = data.val();
                    if(childData.chatname==chatnameinput[1]){ 
                        total_post[total_post.length] = str_before_username+"<strong>" + childData.time+"</strong>"+  "<br>"+childData.email+ "</br>"+childData.data + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    
                }
            });


        })
        .catch(e => console.log(e.message));
        function getTime() {
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            if (h < 10) {
                h = '0' + h;
            }
            if (m < 10) {
                m = '0' + m;
            }
            if (s < 10) {
                s = '0' + s;
            }
            var now = h + ':' + m + ':' + s;
            return now;
        }
}

window.onload = function() {
    init();
};
