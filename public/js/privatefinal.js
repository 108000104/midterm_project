
//private room text new and old are done html is done too.//
function init() {
    var user_email = '';
    var url = location.href;
    var chatnameinput = url.split("?");
    document.getElementById('roomname').innerHTML= chatnameinput[1];

    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');


    // Trace the code first! There are some adjustments...
    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            var Ref = firebase.database().ref('private');
            console.log("test");
            var data = {
                chatname : chatnameinput[1],
                data: post_txt.value,
                email: user_email,
                time : getTime()
            };
           
            Ref.push(data);
            post_txt.value = "";
        }
    });
    
    
    img_btn = document.getElementById('img_btn');

    /// TODO 2: Put the image to storage, and push the image to database's "com_list" node
    ///         1. Get the reference of firebase storage
    ///         2. Upload the image
    ///         3. Get the image file url, the reference of "com_list" and push user email and the image
    


    // The html code for post
    //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0 '></h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray container_1'><strong class='d-block text-gray-dark'>";
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0 container_1 '><strong class='d-block text-gray-dark '></h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray '>";
    
    var str_after_content = "</p></div></div>\n";
    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";

    var postsRef = firebase.database().ref('private');
    // List for store posts html
    var total_post = [];
    var total_post2 = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 3: Adjust the history posts part and the new post part to display both comment and image
            ///         1. Use str_before_img, str_after_img to form the HTML part.
            ///
            ///         Hint: check those we push into the database, some datas can help you.
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(childData.chatname==chatnameinput[1]){ 
                total_post[total_post.length] = str_before_username+"<strong>" + childData.time+"</strong>"+  "<br>"+childData.email+ "</br>"+ str_after_content;
                total_post2 [total_post2.length]= childData.data;}
                first_count += 1;
                
            });
           // document.getElementById('post_list').innerHTML += total_post[i];
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');
            var cur=0;
            //console.log(total_post2[first_count-1+second_count])
            for (var i = 0; i < total_post2.length; i++){
                
                document.getElementsByClassName('container_1')[i].innerText = total_post2[i];
                //console.log( total_post2[i]);
                cur++;
            }

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.chatname==chatnameinput[1]){ 
                    //total_post[total_post.length] = str_before_username + childData.email + "</strong>"  + childData.time + str_after_content;
                    total_post[total_post.length] = str_before_username+"<strong>" + getTime()+"</strong>"+  "<br>"+childData.email+ "</br>"+ str_after_content;
                    total_post2 [total_post2.length]= childData.data;

                    //document.getElementById('post_list').innerHTML += total_post[total_post.length-1];
                   
                    //document.getElementsByClassName('container_1')[second_count-1].innerText = total_post2[second_count-1];
                    
                    document.getElementById('post_list').innerHTML += total_post[total_post.length-1];
                   // document.getElementById('post_list').innerHTML = total_post.join('');
            
                    document.getElementsByClassName('container_1')[total_post2.length-1].innerText = total_post2[total_post2.length-1];
                    }
                    }
                    //console.log('a');
                    //console.log(document.getElementsByClassName('container_1')[second_count-1].innerText);
                    console.log(total_post[total_post.length-1])
            });
                  // console.log(second_count);
                })
           
    
        .catch(e => console.log(e.message));
        function getTime() {
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            if (h < 10) {
                h = '0' + h;
            }
            if (m < 10) {
                m = '0' + m;
            }
            if (s < 10) {
                s = '0' + s;
            }
            var now = h + ':' + m + ':' + s;
            return now;
        }
}

window.onload = function() {
    init();
};
