function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;

            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_name = document.getElementById('count_name');
    post_self = document.getElementById('self_introduction');
    post_infor = document.getElementById('more_information');
    post_user3 = document.getElementById('user3');
    post_user4 = document.getElementById('user4');
    //post_time = document.getElementById('showtime');
    /*
    name:'',
    self:'',
    birthday:'',
    phone:'',
    other:'',
    */
    post_btn.addEventListener('click', function() {
        var Ref = firebase.database().ref('countsetting');
        if (post_name.value != "") {
            var data = {
                name:post_name.value,
                //name:'',
    self:'',
    birthday:'',
    phone:'',
    other:'',
                email: user_email,
                time : getTime(),
                username : 1,
                url:'',
            };
            Ref.push(data);
           /* var data = {
                name:post_name.value,
                //name='',
                self:'hi',
                infor:'100.01.01',
                user3:'0912345678',
                user4:'good :)',
                email: user_email,
                time : getTime(),
                username : 1,
                url:''
            };
            Ref.push(data);*/

        }
        if (post_self.value != "") {
            var data = {
            self : post_self.value,
            name:'',
    //self:'',
    birthday:'',
    phone:'',
    other:'',
            email: user_email,
            time : getTime(),
            username : 1,
            url:'',
            };
            Ref.push(data);
        }
        if (post_infor.value != "") {
            var data = {
                name:'',
                self:'',
                birthday:post_infor.value,
                phone:'',
                other:'',
            email: user_email,
            time : getTime(),
            username : 1,
            url:'',
            };
            Ref.push(data);
        }
        if (post_user3.value != "") {
            var data = {
                name:'',
    self:'',
    birthday:'',
    phone:post_user3.value,
    other:'',
            email: user_email,
            time : getTime(),
            username : 1,
            url:'',
            };
            Ref.push(data);
        }
        if (post_user4.value != "") {
            var data = {
                name:'',
    self:'',
    birthday:'',
    phone:'',
    other:post_user4.value,
            email: user_email,
            time : getTime(),
            username : 1,
            url:'',
            };
            Ref.push(data);
        }
        
        //Ref.push(data);
        post_name.value = "";
        post_self.value ="";
        post_infor.value ="";
        post_user3.value ="";
        post_user4.value ="";
                
    });
    
    
    // The html code for post
    
        function getTime() {
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            if (h < 10) {
                h = '0' + h;
            }
            if (m < 10) {
                m = '0' + m;
            }
            if (s < 10) {
                s = '0' + s;
            }
            var now = h + ':' + m + ':' + s;
            return now;
        }
}
/*
img_btn2 = document.getElementById('img_btn');
    img_btn2.addEventListener('change', function(){
        var file = this.files[0];
        console.log(firebase.storage().ref('countsetting'))
        console.log(firebase.storage().ref(file.name));
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(function(){

            storageRef.getDownloadURL().then(function(url){
                console.log(url);
                var Ref = firebase.database().ref('countsetting');
                var data = {
                    name:'',
                    self:'',
                    birthday:'',
                    phone:'',
                    other:'',
                email: user_email,
                time : getTime(),
                username : 1,
                url: url
                };
                Ref.push(data);
            });
        });
    });*/

window.onload = function() {
    init();
};
