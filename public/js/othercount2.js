function init() {
    var user_email = '';
    var url = location.href;
    var emailinput = url.split("?");
    var otheruser_email = emailinput[1];
    console.log(otheruser_email);
    
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            console.log(user_email);
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logoutbtn = document.getElementById('logout-btn');
            document.getElementsByClassName('count_infor1_1')[0].innerHTML ='self introduction:';
                document.getElementsByClassName('count_infor2_1')[0].innerHTML ='birthday:';
                document.getElementsByClassName('count_infor3_1')[0].innerHTML ='phone:';
                document.getElementsByClassName('count_infor4_1')[0].innerHTML ='other:';
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
           // document.getElementById('post_list').innerHTML = "";
        }
    });


    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0 container_1 '><strong class='d-block text-gray-dark '></h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray '>";
    
    var str_after_content = "</p></div></div>\n";
    var str_before_img = "<img class='img pt-2' style='width: 300px;' src='";
    var str_after_img = "'>";

    var postsRef = firebase.database().ref('countsetting');
    var postsRef_name = firebase.database().ref('countsetting');
    // List for store posts html
    var total_post = [];
    var total_post2 = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    postsRef_name.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childshot) {
                var childData1 = childshot.val();
                console.log(childData1.email);
                if(childData1.email==otheruser_email){
                
                if(childData1.name!='')
                document.getElementsByClassName('count_name')[0].innerHTML = childData1.name;
                if(childData1.self!='')
                document.getElementsByClassName('count_infor1')[0].innerHTML = childData1.self;
                if(childData1.birthday!='')
                document.getElementsByClassName('count_infor2')[0].innerHTML = childData1.birthday;
                if(childData1.phone!='')
                document.getElementsByClassName('count_infor3')[0].innerHTML = childData1.phone;
                if(childData1.other!='')
                document.getElementsByClassName('count_infor4')[0].innerHTML = childData1.other;}
                
                console.log(childData1.name)
                console.log(childData1.self)
                console.log(childData1.birthday)
                console.log(childData1.phone)
                console.log(childData1.other)
            });})
        .catch(e => console.log(e.message));
    
    postsRef.once('value')
        .then(function(snapshot) {
           postsRef.on('child_added', function(data) {
                   var childData = data.val();
                    if(childData.email==otheruser_email){
                        if(childData.url!='')
                            { document.getElementsByClassName('count_photo')[0].innerHTML = "<img class='img pt-2' style='height: 250px;' src='"+childData.url+"'>";
                        
                        }
                    }
                    
                
            });
                 
                })
           
    
        .catch(e => console.log(e.message));
        function getTime() {
            var date = new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            if (h < 10) {
                h = '0' + h;
            }
            if (m < 10) {
                m = '0' + m;
            }
            if (s < 10) {
                s = '0' + s;
            }
            var now = h + ':' + m + ':' + s;
            return now;
        }
}

window.onload = function() {
    init();
};
