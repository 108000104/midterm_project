function init() {
    var user_email = '';
    //var test =new Notification("New message incoming", {
      //  body: "Hi there. How are you doing?"});
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
    post_btn = document.getElementById('post_btn');
    //post_btn2 = document.getElementById('post_btn2');
    post_txt = document.getElementById('comment');

    

    post_btn.addEventListener('click', function() {
    if(post_txt.value != ""){
        console.log(Notification.permission);
        if(post_txt.value>first_count){
            alert("you are wrong!! ");
        }
        else if (Notification.permission === "granted") {
          // var notification = new Notification("Hi");
           alert("to see : "+post_txt.value+"'s information");
           window.location.href='othercount2.html?'+post_txt.value;
        } else if (Notification.permission !== "denied") {
           Notification.requestPermission().then(permission => {
              console.log(permission);
           });
    }
      
   }
    });
    

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>chatroom information </h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('creatroom');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var chatlist =[];
    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(user_email==childData.email){ 
                first_count += 1 
                total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                
                chatlist[first_count] = childData.chatname;
            }
                else if(user_email==childData.user1){
                first_count += 1 
                total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                
                chatlist[first_count] = childData.chatname;
            }
                    
            else if(user_email==childData.user2){
               first_count += 1 
               total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                 
                chatlist[first_count] = childData.chatname;
            }
            else if(user_email==childData.user3){
                first_count += 1 
                total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                
                 chatlist[first_count] = childData.chatname;
             }
             else if(user_email==childData.user4){
                first_count += 1 
                total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                
                 chatlist[first_count] = childData.chatname;
             }
            
            });

            //document.getElementById('post_list').innerHTML = total_post.join('\n');

            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(user_email==childData.email){
                        total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                    }    else if(user_email==childData.user1){
                        total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                    }  
                    else if(user_email==childData.user2){
                        total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                    }
                    else if(user_email==childData.user3){
                        total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                    }
                    else if(user_email==childData.user4){
                        total_post[total_post.length] = str_before_username+'   chatnumber : '+first_count +'    chatname : '+childData.chatname +"</strong>"+ "creater : "+childData.email+"<br>"+'user1 : '+childData.user1 +"<br>"+'user2 : '+childData.user2 +"<br>"+'user3 : '+childData.user3 +"<br>"+'user4 : '+childData.user4 + str_after_content;
                    }  
                    
                }
            });


        })
        .catch(e => console.log(e.message));
}

window.onload = function() {
    init();
};
